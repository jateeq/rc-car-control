% connect the board
a=arduino('COM3')

% initialize pins
disp('Initializing Pins ...');

% specify pin mode for pins 4, 13 and 5
pinMode(a,9,'output');

% output the digital value (0 or 1) to pin 13
digitalWrite(a,9,1);
pause(2);
digitalWrite(a,9,0);

delete(a);
