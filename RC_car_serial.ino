/*
  RC Car Control
  Author: Jawad Ateeq
  June 09, 2014
 */
 
int front_pin = 9;
int right_pin = 10;
int back_pin = 11;
int left_pin = 12;

//(command) 0 is Low, 1 is High
void controlOutput( int input, int command, int tDelay )
{
  switch (input)
  {
    case '1':
      digitalWrite(front_pin, command);
      break;
    case '2':
      digitalWrite(right_pin, command);
      break;
    case '3':
      digitalWrite(back_pin, command);
      break;
    case '4':
      digitalWrite(left_pin, command);
      break;
    default:
      break;
  }
  delay(tDelay);
}

// the setup routine runs once when you press reset:
void setup() {
  
  Serial.begin(9600);

  pinMode(front_pin, OUTPUT); //Makes car go forward
  pinMode(right_pin, OUTPUT);
  pinMode(left_pin, OUTPUT);
  pinMode(back_pin, OUTPUT);
  
}

void loop() {
  
  char input; // command to move the car
  
  while (!Serial.available());
  input = Serial.read();
  Serial.print(input);
  
  controlOutput(input, HIGH, 1000);
  controlOutput(input, LOW, 0);
  
}
  
