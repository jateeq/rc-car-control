This (personal) project aims to autonomously navigate a RC car through random terrains.

An Arduino Duemilanove is being used to push radio commands from a hacked radio controller to the rc car, with the commands being pushed to the arduino via the serial port. 

Presently the commands are manually entered into the Arduino Serial Monitor, but eventually the goal is to run a SLAM algorithm on the computer and push appropriate commands to the Ardunio based on the algorithmic output. 

The sensors used for mapping are cheap IR sensors. I have yet to determine how to pass sensor information back to the laptop.

Note that the MATLAB support package for Arduino (i.e. Arduino I/O) library needs to be downloaded for MATLAB to be able to talk to the Arudino, and the server side service should be running on the Arduino.